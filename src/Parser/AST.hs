module Parser.AST where

type Program = [Pinocchio]

type Pinocchio = (String, Statement)

data Statement
  = Talk Talk
  | Print Print
  | Yes Yes
  | Branch Branch
  | Comp Comp
  | Geppetto Geppetto
  | Empty Empty
  deriving (Show)

type Talk = String
type Print = String
type Yes = Bexp
type Branch = (Bexp, Statement)
type Comp = (Statement, Statement)
type Geppetto = ()
type Empty = ()

data Bexp = Boolean Bool | Comparison Aexp CompOp Aexp
  deriving (Show)

data CompOp = Eq | Neq | Lt | Gt
  deriving (Show, Eq)

data Aexp = Number Int | Nose String
  deriving (Show)
