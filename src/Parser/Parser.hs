module Parser.Parser where

import Control.Applicative (Alternative (..))
import Control.Monad (when)
import Data.Maybe (fromJust, isNothing)
import Parser.AST
import Parser.Helpers

pProgram :: Parser Program
pProgram = many (whitespaced pPinocchio)

pPinocchio :: Parser Pinocchio
pPinocchio = do
  pWord "Pinocchio" <* wsr
  identifier <- pIdentifier
  statement <- whitespaced pBlock
  return (identifier, statement)

pBlock :: Parser Statement
pBlock = pChar '{' *> pStatement <* pChar '}'

pStatement :: Parser Statement
pStatement = pComp <|> fmap (const (Empty ())) ws
  where
    pComp = do
      a <- whitespaced $ pBranch <|> pGeppetto <|> pTalk <|> pYes <|> pPrint
      b <- pStatement <|> return (Empty ())
      return (Comp (a, b))

pBranch :: Parser Statement
pBranch = do
  pWord "if"
  whitespaced $ pChar '('
  expr <- pBexp
  whitespaced $ pChar ')'
  stm <- pBlock
  return (Branch (expr, stm))

pBexp :: Parser Bexp
pBexp = pBoolean <|> pBoolComparison

pBoolean :: Parser Bexp
pBoolean = fmap Boolean (pTrue <|> pFalse)

pBoolComparison :: Parser Bexp
pBoolComparison = do
  expr1 <- pAexp
  op <- whitespaced pComparisonOp
  Comparison expr1 op <$> pAexp

pAexp :: Parser Aexp
pAexp = fmap Number pNumber <|> pNoseExp

pNoseExp :: Parser Aexp
pNoseExp = fmap Nose pIdentifier <* whitespaced (pChar '.') <* pWord "nose"

pComparisonOp :: Parser CompOp
pComparisonOp = fmap f $ pWord "==" <|> pWord "!=" <|> pWord ">" <|> pWord "<"
  where
    f op
      | op == "==" = Eq
      | op == "!=" = Neq
      | op == ">" = Gt
      | otherwise = Lt

pGeppetto :: Parser Statement
pGeppetto = fmap (const (Geppetto ())) $ pWord "Geppetto" <* whitespaced (pWord ".talk()") <* pChar ';'

pTalk :: Parser Statement
pTalk = fmap Talk $ pIdentifier <* whitespaced (pWord ".talk()") <* pChar ';'

pYes :: Parser Statement
pYes = do
  pWord "yes"
  whitespaced $ pChar '('
  expr <- pBexp
  whitespaced $ pChar ')'
  pChar ';'
  return $ Yes expr

pPrint :: Parser Statement
pPrint = do
  pWord "print"
  whitespaced $ pChar '('
  expr <- pIdentifier
  whitespaced $ pChar ')'
  pChar ';'
  return $ Print expr
