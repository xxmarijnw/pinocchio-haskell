module Parser.Helpers where

import Control.Applicative (Alternative (..))
import Data.Char (isAlpha, isDigit, isSpace)
import Data.Functor (($>))

newtype Parser a = Parser
  { runParser :: String -> Maybe (a, String)
  }

instance Functor Parser where
  fmap f p = Parser $ \inp ->
    case runParser p inp of
      Nothing -> Nothing
      Just (v, out) -> Just (f v, out)

instance Applicative Parser where
  pure v = Parser $ \inp -> Just (v, inp)

  pf <*> px = Parser $ \inp ->
    case runParser pf inp of
      Nothing -> Nothing
      Just (f, out) -> runParser (fmap f px) out

instance Monad Parser where
  p >>= f = Parser $ \inp ->
    case runParser p inp of
      Nothing -> Nothing
      Just (v, out) -> runParser (f v) out

instance Alternative Parser where
  empty = Parser $ const Nothing

  p <|> q = Parser $ \inp ->
    case runParser p inp of
      Nothing -> runParser q inp
      Just (v, out) -> Just (v, out)

-- 'ws' is a parser that parses whitespace
ws :: Parser String
ws = many (pSatChar isSpace)

-- 'wsr' is a parser that parses whitespace, and requires at least one space to be present
wsr :: Parser String
wsr = some (pSatChar isSpace)

whitespaced :: Parser a -> Parser a
whitespaced p = ws *> p <* ws

-- 'pIf' is a parser that always parses the first character of the
-- input string
pIf :: Parser Char
pIf = Parser f
  where
    f inp
      | null inp = Nothing
      | otherwise = Just (head inp, tail inp)

-- 'pSatChar' is a parser that only parses the first character of the input
-- string if it satisfies the predicate
pSatChar :: (Char -> Bool) -> Parser Char
pSatChar pred = pIf >>= (\c -> if pred c then return c else empty)

-- 'pChar' is a parser that takes a specific character and only parses that
-- specific character
pChar :: Char -> Parser Char
pChar char = pSatChar (== char)

-- 'pAlpha' is a parser that parses a single alphabetic character
pAlpha :: Parser Char
pAlpha = pSatChar isAlpha

-- 'pDigit' is a parser that parses a single numeric character
pDigit :: Parser Char
pDigit = pSatChar isDigit

-- 'pNumber' is a parser that parses a numeric sequence of characters and
-- converts that to an integer
pNumber :: Parser Int
pNumber = read <$> some pDigit

-- 'pWord' is a parser that takes a string as input and only parses that string
pWord :: String -> Parser String
pWord = mapM pChar

-- 'pIdentifier' is a parser that parses any valid identifier in Pinocchio. An
-- identifier in Pinocchio must only consist of alphabetic characters.
pIdentifier :: Parser String
pIdentifier = some pAlpha

-- 'pTrue' is a parser that parses the boolean "true"
pTrue :: Parser Bool
pTrue = pWord "true" $> True

-- 'pFalse' is a parser that parses the boolean "false"
pFalse :: Parser Bool
pFalse = pWord "false" $> False
