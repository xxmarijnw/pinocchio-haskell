module Main where

import Data.Maybe (fromJust, isNothing)
import Interpreter.Interpreter (interpret)
import Parser.AST (Program)
import Parser.Helpers (Parser (runParser))
import Parser.Parser (pProgram)
import System.Environment (getArgs)

main :: IO ()
main = do
  args <- getArgs
  if null args
    then putStrLn "Usage: ./pinocchio <filename>"
    else interpretFile $ head args

interpretFile :: String -> IO ()
interpretFile file = do
  program <- parse <$> readFile file
  maybe error interpret program
  where error = putStrLn $ "Failed to parse file " ++ file

parse :: String -> Maybe Program
parse src = case runParser pProgram src of
  Just (p, []) -> Just p
  _ -> Nothing
