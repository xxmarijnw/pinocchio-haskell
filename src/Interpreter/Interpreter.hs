module Interpreter.Interpreter where

import Control.Monad (when)
import Data.Char (chr)
import Data.Maybe (fromJust, fromMaybe, isNothing)
import Parser.AST
import Prelude hiding (print)

-- Pname is a Pinocchio name.
type Pname = String

-- Pnose is the length of a Pinocchio's nose.
type Pnose = Int

-- EnvP is the 'Pinocchio environment', which is a function from a Pinocchio
-- name 'p' to the statement that is its body.
type EnvP = Pname -> Statement

-- State contains the values of each Pinocchio's nose, and is a function from
-- the Pinocchio names to nose lengths.
type State = Pname -> Pnose

-- GeppettoStatus indicates whether Geppetto.talk() was called inside a
-- statement. When it is True, it indicates that Geppetto.talk() was called
-- inside S and that that Geppetto call has not been resolved. When it is False,
-- it indicates that Geppetto.talk() was not called inside S, or that all
-- Geppetto calls that occurred in S have already been resolved.
type GeppettoStatus = Bool

-- Output holds everything that has been output by the Pinocchio program.
type Output = IO ()

-- A transition in natural semantics is defined as (S, s) -> s', however, this
-- does not suffice to describe the semantics of Pinocchio. We need to register
-- whether Geppetto.talk() was called inside S and what has been printed.
newtype Transition a = Transition
  { run :: (a, State) -> (State, GeppettoStatus, Output)
  }

interpret :: Program -> IO ()
interpret p = do
  let (_, _, out) = run (program env0) (p, s0)
  out

  where
    env0 = \q -> error $ "Undefined Pinocchio '" ++ q ++ "'"
    s0 = const 0

program :: EnvP -> Transition Program
program envP = Transition t
  where
    t (dp, s) = (s', False, out)
      where
        envP' = updP dp envP
        (s', _, out) = run (statement envP' "main") (envP' "main", s)

statement :: EnvP -> Pname -> Transition Statement
statement envP p = Transition t
  where
    t (stm, s) =
      case stm of
        Talk a -> run (talk envP p) (a, s)
        Print a -> run (print envP p) (a, s)
        Yes a -> run (yes envP p) (a, s)
        Branch a -> run (branch envP p) (a, s)
        Comp a -> run (comp envP p) (a, s)
        Geppetto a -> run (geppetto envP p) (a, s)
        Empty a -> run (empty envP p) (a, s)

talk :: EnvP -> Pname -> Transition Talk
talk envP p = Transition t
  where
    t (q, s) = (s', False, out)
        where
          (s', _, out) = run (statement envP q) (envP q, s)

print :: EnvP -> Pname -> Transition Print
print envP p = Transition t
  where
    t (q, s) = (s, False, putStr [chr (s q)])

yes :: EnvP -> Pname -> Transition Yes
yes envP p = Transition t
  where
    t (bexp, s) = if b then (updS p (\v -> v - 1) s, False, pure ()) else (updS p (+ 1) s, False, pure ())
      where
        b = eBexp bexp s p

branch :: EnvP -> Pname -> Transition Branch
branch envP p = Transition t
  where
    t ((bexp, stm), s) = if b then run (statement envP p) (stm, s) else (s, False, pure ())
      where
        b = eBexp bexp s p

comp :: EnvP -> Pname -> Transition Comp
comp envP p = Transition t
  where
    t ((stm1, stm2), s) = if g then (s', True, out) else (s'', g', out >> out')
      where
        (s', g, out) = run (statement envP p) (stm1, s)
        (s'', g', out') = run (statement envP p) (stm2, s')

geppetto :: EnvP -> Pname -> Transition Geppetto
geppetto envP p = Transition t
  where
    t ((), s) = (s', True, out)
      where
        (s', _, out) = run (statement envP p) (envP p, s)

empty :: EnvP -> Pname -> Transition Empty
empty _ _ = Transition $ \((), s) -> (s, False, pure ())

-- Evaluates the given boolean expression.
eBexp :: Bexp -> State -> Pname -> Bool
eBexp (Boolean b) s p = b
eBexp (Comparison a op b) s p = case op of
  Eq -> a' == b'
  Neq -> a' /= b'
  Lt -> a' < b'
  Gt -> a' > b'
  where
    a' = eAexp a s p
    b' = eAexp b s p

-- Evaluates the given arithmetic expression.
eAexp :: Aexp -> State -> Pname -> Int
eAexp (Number n) _ _ = n
eAexp (Nose q) s p =
  if q == "me"
    then s p
    else s q

-- This function takes a Pinocchio name `p` and a function from Pnose to Pnose
-- and applies that function to `p` in the given state. The way this is done is
-- quite inefficient, but I don't care.
updS :: Pname -> (Pnose -> Pnose) -> State -> State
updS p f s q = if q == p then f $ s p else s q

-- This function takes a program `Dp` and a Pinocchio environment, and gives a
-- new Pinocchio environment with the Pinocchio's declared in `Dp`.
updP :: Program -> EnvP -> EnvP
updP [] envP = envP
updP ((p, stm) : xs) envP = \q -> if q == p then stm else updP xs envP q
